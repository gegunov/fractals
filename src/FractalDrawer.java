import fractals.panels.FractalPanel;

import javax.swing.*;
import java.awt.*;

public class FractalDrawer {

    private JFrame frame;
    private JPanel bottomPanel;
    private JPanel topPanel;
    private FractalPanel centerPanel;

    public FractalDrawer() {
        addFrame();
        addTopPanel();
        addBottomPanel();
        initCentralPanel();
    }

    public static void main(String[] args) {
        new FractalDrawer();
    }

    private void addFrame() {
        frame = new JFrame("Fractals");
        frame.setSize(600, 400);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    private void addTopPanel() {
        topPanel = new JPanel();
        topPanel.add(new JLabel("Top Panel"));
        topPanel.setBackground(Color.GRAY);
        frame.add(topPanel, BorderLayout.NORTH);
    }

    private void initCentralPanel() {
        centerPanel = new FractalPanel();
        centerPanel.setBackground(Color.CYAN);
        centerPanel.setBounds(bottomPanel.getX(), bottomPanel.getHeight(), bottomPanel.getWidth(),
                topPanel.getY() - topPanel.getHeight() - bottomPanel.getHeight());
        frame.add(centerPanel);
    }


    private void addBottomPanel() {
        bottomPanel = new JPanel();
        bottomPanel.setBackground(Color.YELLOW);
        JLabel label = new JLabel("Display fractal");
        bottomPanel.add(label);
        JButton stepForward = new JButton("Step forward");
        stepForward.addActionListener((f) -> {
           centerPanel.stepForward();
        });


        JButton stepBack = new JButton("Step back");
        stepBack.addActionListener((f) -> {
            centerPanel.stepBack();
        });

        bottomPanel.add(stepBack);
        bottomPanel.add(stepForward);
        frame.add(bottomPanel, BorderLayout.SOUTH);
    }
}
