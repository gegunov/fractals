package fractals;

public interface Fractal {
    void draw();
}
