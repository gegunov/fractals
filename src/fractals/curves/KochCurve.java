package fractals.curves;

import fractals.Fractal;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

import static java.lang.Math.cos;
import static java.lang.Math.sin;


public class KochCurve implements Fractal {

    private static final int ANGLE = 45;
    private Graphics2D g;
    private DrawData initialDrawData;

    public  KochCurve(Graphics g, int depth){
        this.g = (Graphics2D) g;
        initialDrawData = new DrawData();
        initialDrawData.setDepth(depth);
        initialDrawData.setLength(1000.0);
        initialDrawData.setAngle(0.0);
        Point point = new Point(400, 600);
        initialDrawData.setStartPoint(point);
    }

    @Override
    public void draw(){
        draw(initialDrawData);
    }

    private void draw(DrawData drawData1){
        int depth = drawData1.getDepth();
        double length = drawData1.getLength();
        double angle = drawData1.getAngle();
        Point2D startPoint = drawData1.getStartPoint();

        if (depth == 0) {
            double x2 = length * cos(angle) + startPoint.getX();
            double y2 = length * sin(angle) + startPoint.getY();
            Shape line = new Line2D.Double(startPoint.getX(), startPoint.getY(), x2, y2);
            System.out.println(String.format("(%f,%f),(%f,%f)", startPoint.getX(), startPoint.getY(), x2, y2));
            g.draw(line);
        } else {
            double newLength = length / 3;
            drawData1.setLength(newLength);
            drawData1.setDepth(depth-1);

            double pt2X = newLength * cos(angle) + startPoint.getX();
            double pt2Y = newLength * sin(angle) + startPoint.getY();
            Point2D pt2 = new Point2D.Double(pt2X, pt2Y);
            DrawData drawData2 = createDrawData(depth -1, angle - ANGLE, newLength, pt2);

            double pt3X = newLength * cos(angle - ANGLE) + pt2.getX();
            double pt3Y = newLength * sin(angle- ANGLE) + pt2.getY();
            Point2D pt3 = new Point2D.Double(pt3X, pt3Y);
            DrawData drawData3 = createDrawData(depth -1, angle + ANGLE, newLength, pt3);

            double pt4X = newLength * cos(angle+ ANGLE) + pt3.getX();
            double pt4Y = newLength * sin(angle+ ANGLE) + pt3.getY();
            Point2D pt4 = new Point2D.Double(pt4X, pt4Y);
            DrawData drawData4 = createDrawData(depth-1, angle, newLength, pt4);

            draw(drawData1);
            draw(drawData2);
            draw(drawData3);
            draw(drawData4);
        }
    }

    private DrawData createDrawData(int depth, double angle, double newLength, Point2D startPoint) {
        DrawData drawData = new DrawData();
        drawData.setAngle(angle);
        drawData.setStartPoint(startPoint);
        drawData.setDepth(depth);
        drawData.setLength(newLength);
        return drawData;
    }


}
