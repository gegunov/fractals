package fractals.curves;

import fractals.Fractal;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class SierpinskyCurve implements Fractal {

    private Graphics2D g;
    private int depth;
    private double dist;

    private Point2D currentPosition;

    public SierpinskyCurve(Graphics g, int depth) {
        this.g = (Graphics2D) g;
        this.depth = depth;
        currentPosition = new Point2D.Double(200, 50);
        dist = 100.0;
    }

    @Override
    public void draw() {
        draw(depth, 10, 10);
    }

    private void draw(int depth, double dx, double dy) {
        drawRight(depth, dx, dy);
        drawLineRelative(dx, dy);
        drawDown(depth, dx, dy);
        drawLineRelative(-dx, dy);
        drawLeft(depth, dx, dy);
        drawLineRelative(-dx, -dy);
        drawUp(depth,dx, dy);
        drawLineRelative(dx, -dy);
    }

    private void drawRight(int depth, double dx, double dy) {
        if(depth > 0){
            depth--;
            drawRight(depth, dx, dy);
            drawLineRelative(dx, dy);
            drawDown(depth, dx, dy);
            drawLineRelative(2 * dx, 0);
            drawUp(depth, dx, dy);
            drawLineRelative(dx, -dy);
            drawRight(depth, dx, dy);
        }
    }

    private void drawDown(int depth, double dx, double dy) {
        if(depth > 0){
            depth--;
            drawDown(depth, dx, dy);
            drawLineRelative(-dx, dy);
            drawLeft(depth, dx, dy);
            drawLineRelative(0, 2 * dy);
            drawRight(depth, dx, dy);
            drawLineRelative(dx, dy);
            drawDown(depth, dx, dy);
        }
    }

    private void drawLeft(int depth, double dx, double dy) {
        if(depth > 0){
            depth--;
            drawLeft(depth, dx, dy);
            drawLineRelative(-dx, -dy);
            drawUp(depth, dx, dy);
            drawLineRelative(-2 * dx, 0);
            drawDown(depth, dx, dy);
            drawLineRelative(-dx, dy);
            drawLeft(depth, dx, dy);
        }
    }

    private void drawUp(int depth, double dx, double dy) {
        if(depth > 0){
            depth--;
            drawUp(depth, dx, dy);
            drawLineRelative(dx, -dy);
            drawRight(depth, dx, dy);
            drawLineRelative(0, -2 * dy);
            drawLeft(depth, dx, dy);
            drawLineRelative(-dx, -dy);
            drawUp(depth, dx, dy);
        }
    }

    private void drawLineRelative(double dx, double dy) {
        double x1 = currentPosition.getX();
        double y1 = currentPosition.getY();
        double x2 = x1 + dx;
        double y2 = y1 + dy;
        System.out.println("(" + x2 + " " + y2 + ")");
        Shape line = new Line2D.Double(x1, y1, x2, y2);
        g.draw(line);
        currentPosition.setLocation(x2, y2);
    }

}
