package fractals.curves;

import fractals.Fractal;

import java.awt.*;
import java.awt.geom.Line2D;
import java.awt.geom.Point2D;

public class HilbertCurve implements Fractal {

    private Graphics2D g;
    private Point2D currentPosition;
    private int depth;

    public HilbertCurve(Graphics g, int depth) {
        this.g = (Graphics2D) g;
        currentPosition = new Point2D.Double(100, 50);
        this.depth = depth;
    }

    @Override
    public void draw() {
        draw(depth, 0, 5);
    }

    private void draw(int depth, double dx, double dy){
        if(depth > 0) {
            draw(depth-1, dy, dx);
        }
        drawLineRelatively(dx, dy);
        if(depth > 0) {
            draw(depth -1, dx, dy);
        }
        drawLineRelatively(dy, dx);
        if(depth > 0) {
            draw(depth-1, dx, dy);
        }
        drawLineRelatively(-dx, -dy);
        if(depth > 0) {
            draw(depth-1, -dy, -dx);
        }

    }
    private void drawLineRelatively(double dx, double dy){
        double x1 = currentPosition.getX();
        double y1 = currentPosition.getY();
        double x2 = x1 + dx;
        double y2 = y1 + dy;
        Shape line = new Line2D.Double(x1, y1, x2, y2);
        g.draw(line);
        currentPosition.setLocation(x2, y2);
    }
}
