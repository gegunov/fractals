package fractals.panels;

import fractals.Fractal;
import fractals.FractalType;

import javax.swing.*;

public class FractalPanel extends JPanel {

    private int depth;
    private Fractal fractal;
    private JList<FractalType> fractalTypes;
    private FractalType selectedFractalType;

    public FractalPanel() {
        addSelectionsList();
    }

    private void addSelectionsList() {
        fractalTypes = new JList<>(FractalType.values());
        fractalTypes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        fractalTypes.addListSelectionListener((selectionEvent) -> {
            super.paint(this.getGraphics());
            depth = 0;
            selectedFractalType = fractalTypes.getSelectedValue();
            fractal = FractalFactory.buildFractal(selectedFractalType, this.getGraphics(), depth);
            fractal.draw();
        });

        this.add(fractalTypes);
    }

    public void stepForward() {
        depth++;
        super.paint(this.getGraphics());
        fractal = FractalFactory.buildFractal(selectedFractalType, this.getGraphics(), depth);
        fractal.draw();
    }

    public void stepBack() {
        if (depth <= 0) {
            return;
        }
        depth--;
        super.paint(this.getGraphics());
        fractal = FractalFactory.buildFractal(selectedFractalType, this.getGraphics(), depth);
        fractal.draw();
    }
}
