package fractals.panels;

import fractals.Fractal;
import fractals.FractalType;
import fractals.curves.HilbertCurve;
import fractals.curves.KochCurve;
import fractals.curves.SierpinskyCurve;

import java.awt.*;

public class FractalFactory {
    public static Fractal buildFractal(FractalType fractalType, Graphics g, int depth) {
        Fractal result;
        switch (fractalType) {
            case KOCH:
                result = new KochCurve(g, depth);
                break;
            case HILBERT:
                result = new HilbertCurve(g, depth);
                break;
            case SIERPINSKY:
                result = new SierpinskyCurve(g, depth);
                break;
            default:
                throw new RuntimeException("Unknown Fractal Type");
        }
        return result;
    }

}
