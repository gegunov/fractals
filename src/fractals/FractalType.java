package fractals;

import java.io.Serializable;

public enum FractalType implements Serializable {
    KOCH, HILBERT, SIERPINSKY
}
